﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RestApi
{
    public class RestApi
    {
        public Task<HttpResponseMessage> DeleteAsJsonAsync<T>(HttpClient httpClient, string requestUri, T data)
            => httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri) { Content = Serialize(data) });
        public Task<HttpResponseMessage> DeleteAsJsonAsync<T>(HttpClient httpClient, string requestUri, T data, CancellationToken cancellationToken)
            => httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri) { Content = Serialize(data) }, cancellationToken);
        public Task<HttpResponseMessage> DeleteAsJsonAsync<T>(HttpClient httpClient, Uri requestUri, T data)
            => httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri) { Content = Serialize(data) });
        public Task<HttpResponseMessage> DeleteAsJsonAsync<T>(HttpClient httpClient, Uri requestUri, T data, CancellationToken cancellationToken)
            => httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Delete, requestUri) { Content = Serialize(data) }, cancellationToken);
        private HttpContent Serialize(object data) => new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
        public Task<HttpResponseMessage> SynchronizePost<T>(HttpClient httpClient, string requestUri, T data)
            => httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Post, requestUri) { Content = Serialize(data) });
        public async Task<string> PostAsJsonAsync<T>(T obj, string postUri)
        {
            using (var client = new HttpClient())
            {
                var response = await client.PostAsJsonAsync(postUri, obj);
                var result = await response.Content.ReadAsStringAsync();
                return result;
            }
        }
        public async Task<string> PostAsJsonAsync(List<T> list, string postUri)
        {
            using (var client = new HttpClient())
            {
                var response = await client.PostAsJsonAsync(postUri, list);
                var result = await response.Content.ReadAsStringAsync();
                return result;
            }
        }
        public async Task<string> PutAsJsonSync<T>(T obj, string putUri)
        {
            using (var client = new HttpClient())
            {
                var response = await client.PutAsJsonAsync(putUri, obj);
                var result = await response.Content.ReadAsStringAsync();
                return result;
            }
        }
        public async Task<string> PutAsJsonSync(List<T> list, string postUri)
        {
            using (var client = new HttpClient())
            {
                var response = await client.PutAsJsonAsync(postUri, list);
                var result = await response.Content.ReadAsStringAsync();
                return result;
            }
        }
        public object GetAsync(string getUri)
        {
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(getUri).Result;
                var result = response.Content.ReadAsAsync<ushort>().Result;
                return result;
            }
        }
        public object GetAsync<T>(T obj, string getUri)
        {
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(getUri + obj).Result;
                var result = response.Content.ReadAsAsync<ushort>().Result;
                return result;
            }
        }
    }
}
